﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using UnitTest.Controllers;
using UnitTest.Models;
using UnitTest.Repository;
using Xunit;

namespace XUnitTestProject
{
    public class UserControllerUnitTest
    {
        [Fact]
        public async Task Values_Get_All()
        {
            // Arrange
            var controller = new UserController(new UserRepository());

            // Act
            var result = await controller.Get();

            // Assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var users = okResult.Value.Should().BeAssignableTo<IEnumerable<User>>().Subject;

            users.Count().Should().Be(50);
        }

        [Fact]
        public async Task Values_Get_Specific()
        {
            // Arrange
            var controller = new UserController(new UserRepository());

            // Act
            var result = await controller.Get(16);

            // Assert
            var okResult = result.Should().BeOfType<OkObjectResult>().Subject;
            var user = okResult.Value.Should().BeAssignableTo<User>().Subject;
            user.Id.Should().Be(16);
        }

        [Fact]
        public async Task Users_Add()
        {
            // Arrange
            var controller = new UserController(new UserRepository());
            var newUser = new User
            {                
                Username = "Bao Bao",
                Password = "1111111",
                Email = "baobao@gmail.com"
            };

            // Act
            var result = await controller.Post(newUser);

            // Assert
            var okResult = result.Should().BeOfType<CreatedAtActionResult>().Subject;
            var user = okResult.Value.Should().BeAssignableTo<User>().Subject;
            user.Id.Should().Be(51);
        }

        [Fact]
        public async Task Users_Change()
        {
            // Arrange
            var repo = new UserRepository();
            var controller = new UserController(repo);
            var newUser = new User
            {
                Username = "Bao Bao",
                Password = "1111111",
                Email = "baobao@gmail.com"
            };

            // Act
            var result = await controller.Put(20, newUser);

            // Assert
            var okResult = result.Should().BeOfType<NoContentResult>().Subject;

            var user = repo.GetById(20);
            user.Id.Should().Be(20);
            user.Username.Should().Be("Bao Bao");            
            user.Password.Should().Be("1111111");
            user.Email.Should().Be("baobao@gmail.com");
        }

        [Fact]
        public async Task Users_Delete()
        {
            // Arrange
            var repo = new UserRepository();
            var controller = new UserController(repo);

            // Act
            var result = await controller.Delete(20);

            // Assert
            var okResult = result.Should().BeOfType<NoContentResult>().Subject;

           
        }
    }
}
