﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using UnitTest.Models;
using UnitTest.Repository;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace UnitTest.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private IUserRepository _IUserRepo;

        public UserController(IUserRepository userRepository)
        {
            _IUserRepo = userRepository;
        }
        // GET api/user
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var models = _IUserRepo.GetAll();

            return Ok(models);
        }

        // GET api/user/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            var model = _IUserRepo.GetById(id);

            return Ok(model);
        }

        // POST api/user
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]User model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = _IUserRepo.Insert(model);

            return CreatedAtAction("Get", new { id = user.Id }, user);
        }

        // PUT api/user/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody]User model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _IUserRepo.Update(id, model);

            return NoContent();
        }

        // DELETE api/user/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            _IUserRepo.Delete(id);
            return NoContent();
        }
    }
}