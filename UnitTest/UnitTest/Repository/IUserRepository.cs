﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnitTest.Models;

namespace UnitTest.Repository
{
    public interface IUserRepository
    {
        IEnumerable<User> GetAll();
        User GetById(int id);
        User Insert(User user);
        void Update(int id, User user);
        void Delete(int id);
    }
}
