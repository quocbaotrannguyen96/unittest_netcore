﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnitTest.Models;
using GenFu;

namespace UnitTest.Repository
{
    public class UserRepository: IUserRepository
    {
        private List<User> Users { get; set; }

        public UserRepository()
        {
            var i = 0;
            Users = A.ListOf<User>(50);
            Users.ForEach(user =>
            {
                i++;
                user.Id = i;
            });
        }

        public IEnumerable<User> GetAll()
        {
            return Users;
        }

        public User GetById(int id)
        {
            return Users.First(_ => _.Id == id);
        }

        public User Insert(User user)
        {
            var newid = Users.OrderBy(_ => _.Id).Last().Id + 1;
            user.Id = newid;

            Users.Add(user);

            return user;
        }

        public void Update(int id, User user)
        {
            var existing = Users.First(_ => _.Id == id);            
            existing.Username = user.Username;            
            existing.Password = user.Password;
            existing.Email = user.Email;
        }

        public void Delete(int id)
        {
            var existing = Users.First(_ => _.Id == id);
            Users.Remove(existing);
        }
    }

    
}

